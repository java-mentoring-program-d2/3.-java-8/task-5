package clazz;

import collectors.MyCollectorParallel;
import collectors.MyCollectorSequental;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class AClass {
    private final String name;
    private final int age;
    private final double rate;

    public static void main(String[] args) {
        List<AClass> list = new ArrayList<>();
        for (int i = 0; i < 1_000_000; i++) {
            list.add(new AClass("name" + i, i, i / 2.0 + 1));
        }

        LocalTime c1start = LocalTime.now();
        String collect1 = list.stream()
                .collect(new MyCollectorSequental());
        LocalTime c1end = LocalTime.now();
        System.out.println("Execution time without paralleling : " + Duration.between(c1end, c1start));

        LocalTime c2start = LocalTime.now();
        String collect2 = list.stream()
                .collect(new MyCollectorParallel());
        LocalTime c2end = LocalTime.now();
        System.out.println("Execution time with paralleling : " + Duration.between(c2end, c2start));
    }

    public AClass(String name, int age, double rate) {
        this.name = name;
        this.age = age;
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getRate() {
        return rate;
    }

    public double firstMethod() {
        return age * rate;
    }

    public double secondMethod() {
        return (name.length() + age) * rate;
    }

    @Override
    public String toString() {
        return "A{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", rate=" + rate +
                '}';
    }
}

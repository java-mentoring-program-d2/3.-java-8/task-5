package collectors;

import clazz.AClass;

import java.util.ArrayList;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public abstract class MyCollector implements Collector<AClass, ArrayList<Integer>, String> {

    @Override
    public Supplier<ArrayList<Integer>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<ArrayList<Integer>, AClass> accumulator() {
        return (list, a) -> list.add((int) a.firstMethod());
    }

    @Override
    public BinaryOperator<ArrayList<Integer>> combiner() {
        return (list1, list2) -> {
            list1.addAll(list2);
            return list1;
        };
    }

    @Override
    public Function<ArrayList<Integer>, String> finisher() {
        return list -> {
            final StringBuilder sb = new StringBuilder("");
            list.forEach((sb::append));
            return sb.toString();
        };
    }

    @Override
    public abstract Set<Collector.Characteristics> characteristics();
}

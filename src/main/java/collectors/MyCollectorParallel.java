package collectors;

import java.util.HashSet;
import java.util.Set;

public class MyCollectorParallel extends MyCollector {

    @Override
    public Set<Characteristics> characteristics() {
        Set<Characteristics> set = new HashSet<>();
        set.add(Characteristics.UNORDERED);
        set.add(Characteristics.CONCURRENT);
        return set;
    }
}
